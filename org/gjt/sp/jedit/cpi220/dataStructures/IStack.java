package org.gjt.sp.jedit.cpi220.dataStructures;

import java.util.Iterator;

public interface IStack<Item> {

	/**
	 * Is this stack empty?
	 * @return true if this stack is empty; false otherwise
	 */
	public abstract boolean isEmpty();

	/**
	 * Adds the item to this stack.
	 * @param item the item to add
	 */
	public abstract void push(Item item);

	/**
	 * Removes and returns the item most recently added to this stack.
	 * @return the item most recently added
	 * @throws java.util.NoSuchElementException if this stack is empty
	 */
	public abstract Item pop();
	
	public abstract Item peek();
	
	public abstract int size();
	
	public abstract Iterator<Item> iterator();

}